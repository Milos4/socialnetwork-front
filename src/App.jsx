import logo from './logo.svg';
import './App.css';
import axios from 'axios';
import Login from './component/Login';
import LoginPage from './pages/LoginPage';
import ProfilePage from './pages/ProfilePage';
import Profile1Page from './pages/Profile1Page';
import { BrowserRouter as Router, Routes  ,Route, Link } from 'react-router-dom'
import Home from './component/Home';
import HomePage from './pages/HomePage';
import RegisterPage from './pages/RegisterPage';
import FriendRequestPage from './pages/FriendRequestPage';
import EditProfile from './component/EditProfile';


function App() {
  return (
   // <div className="App">
    <Router>
      <Routes>
        <Route exact path='/login' element={<div className="App"><LoginPage/></div>} />
        <Route exact path='/home' element={<HomePage/>} />
        <Route exact  path='/profile/:id' element={<ProfilePage/>} />
        <Route exact  path='/profile1/:id1' element={<Profile1Page/>} />
        <Route exact  path='/register' element={<div className="App"><RegisterPage/></div>} />
        <Route exact  path='/friend' element={<div className="App"><FriendRequestPage/></div>} />
        <Route exact  path='/update/:id' element={<div className="App"><EditProfile/></div>} />
      </Routes>
    </Router>
  );
}

export default App;
