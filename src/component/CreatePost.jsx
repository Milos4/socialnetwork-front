import Box from '@mui/material/Box';
import TextField from '@mui/material/TextField';
import Button from '@mui/material/Button';
import axios from '../api/axios';
import {useNavigate} from 'react-router-dom'
import { useState } from 'react';

const AddPost_Url = 'posts/add'


const CreatePost = () => {
    const [content , setContent] = useState('')

    let navigate = useNavigate();

    const addPost = (e) => {
        const ulogovan = localStorage.getItem("Id")
        const ulogovanId = parseInt(ulogovan);
        e.preventDefault();
        axios.post(AddPost_Url, {
            id : 0,
            accountId : ulogovanId,
            content ,
            time : "2022-05-24T08:01:27"
        }).then(res => {
          window.location.reload(false);
            console.log(res); }).catch(err => console.log(err))       
    }

    

    return (
        <Box
        component="form"
        sx={{
          '& .MuiTextField-root': { m: 1 , width: '50ch'},
        }}
        noValidate
        autoComplete="off"
        alignItems="center"
        justifyContent="center"
        display="flex"
        >
        <div>
            <TextField
            id="newPost"
            label="Post"
            defaultValue=""
            onChange={(e) => setContent(e.target.value)}
            value={content}
            />
            <br/>
            <Button
                onClick={addPost}
                variant="contained"
                sx={{ mt: 0.2, mb: 2 }}
                >
              Create post
            </Button>
        </div>
          
    </Box>
    );
  }

  export default CreatePost;

