import { useEffect , useState } from 'react';
import Button from '@mui/material/Button';
import TextField from '@mui/material/TextField';
import axios  from '../api/axios';
import AuthContext from '../context/AuthProvider';
import {useNavigate} from 'react-router-dom'

const AccEdit_Url = 'accounts/update/'
const Acc_Url = 'accounts/'
const EditProfile = () => {

    
    let navigate = useNavigate();

    const [acc, setAcc]=useState("");
    const ulogovan = localStorage.getItem("Id")
    const ulogovanId = parseInt(ulogovan);
  

    const getAcc = async () => {
        const response = await axios.get(Acc_Url + ulogovan);    
        return response.data   
    }

 
    const [username,setUsername] = useState('');
    const [password,setPassword] = useState(acc.password);
    const [firstName,setFirstName] = useState('');
    const [lastName,setLastName] = useState('');
    const [email,setEmail] = useState('');
    const [phone,setPhone] = useState('');

   

    useEffect(() => {
        const getAcc1 = async() => {
            const acc = await getAcc();
            setAcc(acc)
            setUsername(acc.username)
            setFirstName(acc.firstName)
            setLastName(acc.lastName)
            setEmail(acc.email)
            setPhone(acc.phone)
            
        };
        getAcc1();
    },[]);
    const accId = parseInt(acc.id);

    const update = (e) => {
        e.preventDefault();
        axios.put(AccEdit_Url + ulogovan, {
            id : accId ,
            username : username,
            password : "123" ,
            firstName : firstName ,
            lastName : lastName ,
            email : email ,
            phone : phone ,
            picture : "" 
        }).then(res => {
            console.log(accId);
            navigate("/home")
        }).catch(err => console.log(acc.password))       
    }
    return(
        <div>
        <h1>
            Edit Profile
        </h1>
        <TextField 
              margin="normal"
              required
              id="username"
              name="username"
              onChange={(e) => setUsername(e.target.value)}
              value={username}
              type ="text"
            />
            <br/>
            <TextField 
              margin="normal"
              required
              id="firstName"
              name="firstName"
              onChange={(e) => setFirstName(e.target.value)}
              value={firstName}
              type ="text"
            />
            <br/>
            <TextField 
              margin="normal"
              required
              id="lastName"
              name="lastName"
              onChange={(e) => setLastName(e.target.value)}
              value={lastName}
              type ="text"
            />
            <br/>
            <TextField 
              margin="normal"
              required
              id="email"
              name="email"
              onChange={(e) => setEmail(e.target.value)}
              value={email}
              type ="text"
            />
            <br/>
            <TextField 
              margin="normal"
              required
              id="phone"
              name="phone"
              onChange={(e) => setPhone(e.target.value)}
              value={phone}
              type ="text"
            />
            <br/>
            <Button
                onClick={update}
                variant="contained"
                sx={{ mt: 3, mb: 2 }}
                >
              Edit
            </Button>
    </div>
    );
}

export default EditProfile;