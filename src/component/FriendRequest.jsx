import React from "react";
import { useEffect, useRef, useState ,useContext} from 'react';
import axios  from '../api/axios';
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell, { tableCellClasses } from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableRow from '@mui/material/TableRow';
import DeleteIcon from '@mui/icons-material/Delete';
import {useNavigate , Link} from 'react-router-dom'
import { Button } from "@mui/material";
import "../App.css"

import Paper from '@mui/material/Paper';

const Request_Url = 'friendships/receiver/'
const AcceptRequest_Url = '/friendships/update/'
const DeclineRequest_Url = '/friendships/delete/'

function FriendRequest(){
    const ulogovan =localStorage.getItem("Id");
    const ulogovanId = parseInt(ulogovan);

    const [friendrequest, setFriendrequest]=useState([]);


    const allFriendsrequest = async () => {
        const response = await axios.get(Request_Url + ulogovan);    
        return response.data
        
    }

    useEffect(() => {
        const getFriendsReq = async() => {
            const allReq = await allFriendsrequest();
            setFriendrequest(allReq)
        };
        getFriendsReq();
    },[]);

    const declineFs = async(id) => {
      let data = await axios.delete(DeclineRequest_Url + id)
      window.location.reload(false);
    }

    const acceptFs = async(id,senderId) => {
      await axios.put(AcceptRequest_Url + id ,
      {
        id : id,
        senderId : senderId,
        receiverId : ulogovanId,
        status : 'Accepted'
    }).then(res => {
      window.location.reload(false);
        console.log(res); }).catch(err => console.log(err))    
    }


    return (
        <div className="div-home">
        <TableContainer component={Paper}>
        <Table sx={{ minWidth: 700 }} aria-label="customized table">
          <TableBody>
            {friendrequest.map((row) => (
              <TableRow key={row.name} >  
                <TableCell align="right">Friend request from:<br/>{row.usernameSender}</TableCell>
                <TableCell align="right">Status: {row.status}</TableCell>
                <TableCell align="right" ><Button mt={2} variant="outlined" onClick={()=> acceptFs(row.id,row.senderId)}>Accept</Button><Button mt={2} variant="outlined" onClick={()=> declineFs(row.id)} >Decline</Button> </TableCell>
              </TableRow>
            ))}
          </TableBody>
        </Table>
      </TableContainer>
      </div>
    );
}

export default FriendRequest