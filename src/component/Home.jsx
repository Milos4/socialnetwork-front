import React from "react";
import { useEffect, useRef, useState ,useContext} from 'react';
import axios  from '../api/axios';
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell, { tableCellClasses } from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableRow from '@mui/material/TableRow';
import {useNavigate , Link} from 'react-router-dom'
import Button from '@mui/material/Button';
import "../App.css"

import Paper from '@mui/material/Paper';
const Post_Url = 'posts/for/'
const DeleteLike_Url = '/likes/delete/'
const AddLike_Url = '/likes/add '

const Home = () => {

    const [posts, setPosts]=useState([]);
    const ulogovan = localStorage.getItem("Id")
    const ulogovanId = parseInt(ulogovan);

    const allPosts1 = async () => {
        const response = await axios.get(Post_Url + ulogovanId);    
        return response.data
        
    }


    useEffect(() => {
        const getAllPosts = async() => {
            const allPosts = await allPosts1();
            console.log(allPosts)
            setPosts(allPosts)
            console.log(posts)
        };
        getAllPosts();
    },[]);

    const like = async(like,id)=> {
      if (like === "false"){
        await axios.post(AddLike_Url,
          {
            id : 0,
            "accountId": ulogovanId,
            "postId": id

          }).then(res => {
            window.location.reload(false);
         }).catch(err => console.log(err))     

      }else {
        let data = await axios.delete(DeleteLike_Url + id +'/' + ulogovan)
        console.log(DeleteLike_Url + id +'/' + ulogovan)
        window.location.reload(false);
      }
    }
    

return (
<div className="div-home">
        <TableContainer component={Paper}>
        <Table sx={{ minWidth: 700 }} aria-label="customized table">
          <TableBody>
            {posts.map((row) => (
              <TableRow key={row.name} >
                <TableCell align="right"><Link to={`/profile1/${row.accId}`}>Username: {row.username}</Link> <br/> Time: {row.time}</TableCell>
                <TableCell align="right">Content:{row.content}</TableCell>
                <TableCell align="right">Likes:{row.likes}</TableCell>
                <TableCell align="right" onClick={() => like(row.isLike.toString(),row.id)}>
                {(() => {
              if (row.isLike.toString() == 'false'){
                  return (
                      <Button>Like</Button>
                  )
              }
              return <Button>Dislike</Button>;
            })()}
            </TableCell>
              </TableRow>
            ))}
          </TableBody>
        </Table>
      </TableContainer>
      </div>
  );
}

export default Home