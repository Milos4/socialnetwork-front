import '../App.css';
import Button from '@mui/material/Button';
import TextField from '@mui/material/TextField';
import { useEffect, useRef, useState ,useContext} from 'react';
import axios  from '../api/axios';
import AuthContext from '../context/AuthProvider';
import {useNavigate} from 'react-router-dom'

const Login_Url = 'accounts/login'

const Login = () => {
    const {setAuth}=useContext(AuthContext)
    
    const userRef = useRef();
    const errRef  = useRef();

    const [user,setUser] = useState('');
    const [pass,setPass] = useState('');
    const [errMsg,setErrMsg] = useState('');
    const [success,setSuccess] = useState(false);

    let navigate = useNavigate();
    
    useEffect(() => {
        userRef.current.focus();
    },[])

    useEffect(() => {
        setErrMsg('');
    },[user,pass])

    const login = async (e) => {
        e.preventDefault();
        
        try{
            const response = await axios.post(Login_Url,JSON.stringify({username: user,password: pass}),
                {
                    headers:{'Content-Type': 'application/json'},
                    withCredentials: true
                }
            );
          //  console.log(JSON.stringify(response?.data.id));
            const accId  = JSON.stringify(response?.data.id);
            console.log(accId)
          //  console.log(user)
         //   navigate('/home');
         //   setAuth({accId,user})
            setUser('');
            setPass('');
            localStorage.setItem("Id", accId)
            localStorage.setItem("name", user)
            setSuccess(true);
            navigate('/home');
        }catch(err){
            if(!err?.response){
                setErrMsg('No Server Respones')
            }else if (err.response?.status === 400){
                setErrMsg('Missing Username or Password');
            }else if (err.response?.status === 401){
                setErrMsg('Unauthorized');
            }else{
                setErrMsg('Login Failed')
            }
        } 
    }
    return(

    <div>
        <h1>
            Login Page
        </h1>
        <TextField 
              margin="normal"
              required
              id="username"
              label="Username"
              name="username"
              ref={userRef}
              onChange={(e) => setUser(e.target.value)}
              value={user}
              type ="text"
            />
            <br/>
            <TextField
              margin="normal"
              required
              name="password"
              label="Password"
              type="password"
              id="password"
              onChange={(e) => setPass(e.target.value)}
              value={pass}
              />
            <br/>
            <Button
                onClick={login}
                variant="contained"
                sx={{ mt: 3, mb: 2 }}
                >
              Sign In
            </Button>
    </div>
    )
}
export default Login