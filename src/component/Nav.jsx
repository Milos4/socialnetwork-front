
import '../App.css';

import { Link } from 'react-router-dom'

function Nav(){
    const navStyle = {
        color:'white'
    };
    const ulogovan =localStorage.getItem("name");
    const ulogovanId =localStorage.getItem("Id");

    return (
        <nav>
             <Link style={navStyle} to={`/profile/${ulogovanId}`}><h1>Profile: {ulogovan}</h1></Link>
            <ul className='nav-links'>
                <Link style={navStyle} to="/home">
                    <li>Home</li>
                </Link>
                <Link style={navStyle} to="/friend">
                    <li>Freind request</li>
                </Link>
                <Link style={navStyle} to="/login">
                    <li>Logout</li>
                </Link>
            </ul>
        </nav>
    );
}

export default Nav