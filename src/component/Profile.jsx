import React from "react";
import { useEffect, useState } from 'react';
import axios  from '../api/axios';
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import DeleteIcon from '@mui/icons-material/Delete';
import EditIcon from '@mui/icons-material/Edit';
import TableRow from '@mui/material/TableRow';
import { Box } from "@mui/system";

import Paper from '@mui/material/Paper';
import { Button } from "@mui/material";
import { useNavigate ,useParams } from "react-router-dom";
const Post_Url = 'postsacc/'
const DeletePost_Url = 'posts/delete/'
const Acc_Url = 'accounts/'

const Profile = () => {
  
  let navigate = useNavigate();

  const { id } = useParams();

  const [posts, setPosts]=useState([]);
  const [acc, setAcc]=useState("");
  const ulogovan = localStorage.getItem("Id")
  const ulogovanId = parseInt(ulogovan);
  
    const allPosts1 = async () => {
        const response = await axios.get(Post_Url + id);    
        console.log(Post_Url + id)
        return response.data
    }

    useEffect(() => {
        const getAllPosts = async() => {
            const allPosts = await allPosts1();
            const acc = await getAcc();
            setPosts(allPosts)
            setAcc(acc)
        };
        getAllPosts();
    },[]);

    const deletePost = async(id) => {
      let data = await axios.delete(DeletePost_Url + id)
      window.location.reload(false);
    }

    const getAcc = async () => {
      const response = await axios.get(Acc_Url + ulogovan);    
      return response.data   
  }

    const editPost = async(id) => {
      //
    }

    const editAcc = async() => {
      navigate("../update/"+ulogovan)
    }
    
return (
    <div>
        <Box>
          <h2>Username: {acc.username} <Button variant="contained" onClick={editAcc}>Edit profile</Button></h2>
          <h3>First name: {acc.firstName}</h3>
          <h3>Last name: {acc.lastName}</h3>
          <h3>Email: {acc.email}</h3>
          <h3>Phone: {acc.phone}</h3>
          <br/>
        </Box>
        <TableContainer component={Paper}>
        <Table sx={{ minWidth: 700 }} aria-label="customized table">
          <TableBody>
            {posts.map((row) => (
              <TableRow key={row.name}>
                <TableCell align="right">{row.id}</TableCell>
                <TableCell align="right">Username: {row.username} <br/> Time: {row.time}</TableCell>
                <TableCell align="right">Content: {row.content}</TableCell>
                <TableCell align="right">Likes: {row.likes}</TableCell>
                <TableCell align="right">Liked: {row.isLike.toString()}</TableCell>
                <TableCell><Button variant="outlined"  startIcon={<EditIcon />} onClick={()=> editPost(row.id)}>Edit</Button>           <Button variant="outlined"  startIcon={<DeleteIcon />} onClick={()=> deletePost(row.id)}>Delete</Button></TableCell>
              </TableRow>
            ))}
          </TableBody>
        </Table>
      </TableContainer>
      </div>
  );
}
export default Profile