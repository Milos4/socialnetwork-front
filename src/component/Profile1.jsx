import React from "react";
import { useEffect, useState } from 'react';
import axios  from '../api/axios';
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import DeleteIcon from '@mui/icons-material/Delete';
import EditIcon from '@mui/icons-material/Edit';
import TableRow from '@mui/material/TableRow';
import Input from '@mui/material/Input';
import Typography from '@mui/material/Typography';


import Paper from '@mui/material/Paper';
import { Button, TextField } from "@mui/material";
import {  useParams } from "react-router-dom";
import { Box } from "@mui/system";
const Post_Url = 'postsacc/'
const Acc_Url = 'accounts/'
const Exist_Url = '/friendships/exist/'  
const SendReq_Url = '/friendships/add'  

const Profile1 = () => {

  const  { id1 } = useParams();

  const [posts, setPosts]=useState([]);
  const [acc, setAcc]=useState("");
  const [existt, setExistt]=useState("");



  const ulogovan = localStorage.getItem("Id")
  const ulogovanId = parseInt(ulogovan);
    const allPosts1 = async () => {
        const response = await axios.get(Post_Url + id1);   
        console.log(Post_Url + id1) 
        return response.data
        
    }

    const getAcc = async () => {
      const response = await axios.get(Acc_Url + id1);    
      return response.data   
  }

  const exist = async () => {
    const response = await axios.get(Exist_Url + id1 + '/' + ulogovan);   
    return response.data   
   
}
    useEffect(() => {
        const getAllPostsAndAcc = async() => {
            const allPosts = await allPosts1();
            const acc = await getAcc();
            const ex = await exist();

            setPosts(allPosts)
            setAcc(acc)
            setExistt(ex)
            console.log(ex)
        };
        getAllPostsAndAcc();
    },[]);

    const send = async(id)=> {
      await axios.post(SendReq_Url,
        {
          id : 0,
          "senderId": ulogovanId,
          "receiverId": id,
          "status": "Pending"
        }).then(res => {
          window.location.reload(false);
       }).catch(err => console.log(err))     
  }

return (
    <div>
        <Box>
          <h2>Username: {acc.username}</h2>
          <h3>First name: {acc.firstName}</h3>
          <h3>Last name: {acc.lastName}</h3>
          <h3>Email: {acc.email}</h3>
          <br/>
        </Box>
        {(() => {
              if (existt != true){
                  return (
                      <Button variant="contained" onClick={() => send(acc.id)} >Send friend request</Button>
                  )
            }
        return (
        <TableContainer component={Paper}>
        <Table sx={{ minWidth: 700 }} aria-label="customized table">
          <TableBody>
            {posts.map((row) => (
              <TableRow key={row.name}>
                <TableCell align="right">Username: {row.username} <br/> Time: {row.time}</TableCell>
                <TableCell align="right">Content: {row.content}</TableCell>
                <TableCell align="right">Likes: {row.likes}</TableCell>
                <TableCell align="right">Liked: {row.isLike.toString()}</TableCell>
              </TableRow>
            ))}
          </TableBody>
        </Table>
      </TableContainer>
       )})()}
      </div>
  );
}
export default Profile1