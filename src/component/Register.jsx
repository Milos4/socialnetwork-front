import { useEffect , useState } from 'react';
import Button from '@mui/material/Button';
import TextField from '@mui/material/TextField';
import axios  from '../api/axios';
import AuthContext from '../context/AuthProvider';
import {useNavigate} from 'react-router-dom'

const AddAccount_Url = 'accounts/add'
const Register = () => {

    let navigate = useNavigate();

    const [username,setUsername] = useState('');
    const [password,setPassword] = useState('');
    const [firstName,setFirstName] = useState('');
    const [lastName,setLastName] = useState('');
    const [email,setEmail] = useState('');
    const [phone,setPhone] = useState('');

    const register = (e) => {
        e.preventDefault();
        axios.post(AddAccount_Url, {
            id : 0,
            username : username,
            password : password ,
            firstName : firstName ,
            lastName : lastName ,
            email : email ,
            phone : phone ,
            picture : "" ,
        }).then(res => {
            console.log(res);
            navigate("/login")
        }).catch(err => console.log(err))       
    }
    return(
        <div>
        <h1>
            Register
        </h1>
        <TextField 
              margin="normal"
              required
              id="username"
              label="Username"
              name="username"
              onChange={(e) => setUsername(e.target.value)}
              value={username}
              type ="text"
            />
            <br/>
            <TextField
              margin="normal"
              required
              name="password"
              label="Password"
              type="password"
              id="password"
              onChange={(e) => setPassword(e.target.value)}
              value={password}
              />
            <br/>
            <TextField 
              margin="normal"
              required
              id="firstName"
              label="FirstName"
              name="firstName"
              onChange={(e) => setFirstName(e.target.value)}
              value={firstName}
              type ="text"
            />
            <br/>
            <TextField 
              margin="normal"
              required
              id="lastName"
              label="LastName"
              name="lastName"
              onChange={(e) => setLastName(e.target.value)}
              value={lastName}
              type ="text"
            />
            <br/>
            <TextField 
              margin="normal"
              required
              id="email"
              label="Email"
              name="email"
              onChange={(e) => setEmail(e.target.value)}
              value={email}
              type ="text"
            />
            <br/>
            <TextField 
              margin="normal"
              required
              id="phone"
              label="Phone"
              name="phone"
              onChange={(e) => setPhone(e.target.value)}
              value={phone}
              type ="text"
            />
            <br/>
            <Button
                onClick={register}
                variant="contained"
                sx={{ mt: 3, mb: 2 }}
                >
              Sign In
            </Button>
    </div>
    );
}

export default Register;