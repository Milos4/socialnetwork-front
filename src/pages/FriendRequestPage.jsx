import FriendRequest from "../component/FriendRequest";
import Nav from "../component/Nav";


function FriendRequestPage(){
    const ulogovan =localStorage.getItem("name");

    return (
        <div>
            <Nav/>
            <FriendRequest/>
        </div>
    );
}

export default FriendRequestPage