import CreatePost from "../component/CreatePost";
import Home from "../component/Home"
import Nav from "../component/Nav";

function HomePage() {
    return (
        <div>
            <Nav/>
            <CreatePost/>
            <Home/>
        </div>
    );
  }

  export default HomePage;
