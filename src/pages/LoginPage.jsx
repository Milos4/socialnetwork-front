import { Button } from "@mui/material";
import Login from "../component/Login"
import {useNavigate} from 'react-router-dom'

function LoginPage() {

  let navigate = useNavigate(); 

  const routeChange = () =>{ 
  let path = `/Register`; 
  navigate(path);
  }
  return (
    <div>
      <Login/>
      <Button onClick={routeChange}>Register</Button>
    </div>
    );
  }

  export default LoginPage;
