//import Home from "../component/Home"
import Profile1 from "../component/Profile1";
import Nav from "../component/Nav";
import { useParams } from "react-router-dom";

function Profile1Page() {

    const { id1 } = useParams();

    return (
        <div>
            <Nav/>
            <Profile1/>
        </div>
    );
  }

  export default Profile1Page;
