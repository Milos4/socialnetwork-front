//import Home from "../component/Home"
import Profile from "../component/Profile";
import Nav from "../component/Nav";
import { useParams } from "react-router-dom";

function ProfilePage() {

    const { id } = useParams();

    return (
        <div>
            <Nav/>
            <Profile/>
        </div>
    );
  }

  export default ProfilePage;
